﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;

namespace NumberUtilities
{
    public class PrimeGenerator
    {
        public long LastPrime { get; private set; }
        public Timer IntervalTimer;
        public Timer LimitTimer;

        public void StartCalculatingPrimes()
        {
            //start gathering primes
            foreach (var x in PrimeNumbersWithLinq())
            {
                //store the last calculated prime in a field, accessible by the interval event method
                LastPrime = x;
            }
        }


        #region Optimized Method
        //https://handcraftsman.wordpress.com/2010/09/02/ienumerable-of-prime-numbers-in-csharp/
        public IEnumerable<long> PrimeNumbersWithLinq()
        {
            var memoized = new List<long>();
            long sqrt = 1;
            var primes = PotentialPrimes().Where(x =>
            {
                sqrt = GetSqrtCeiling(x, sqrt);
                return memoized.TakeWhile(y => y <= sqrt).All(y => x % y != 0);
            });
            foreach (long prime in primes)
            {
                yield return prime;
                memoized.Add(prime);
            }
        }

        private long GetSqrtCeiling(long value, long start)
        {
            while (start * start < value)
            {
                start++;
            }
            return start;
        }

        private IEnumerable<long> PotentialPrimes()
        {
            yield return 2;
            yield return 3;
            var k = 1;
            while (LimitTimer.Enabled)
            {
                yield return k * 6 - 1;
                yield return k * 6 + 1;
                k++;
            }
        }
        #endregion



        #region Simple Method
        public IEnumerable<long> PrimeNumbers()
        {
            var result = 3;
            yield return 2;

            while (LimitTimer.Enabled)
            {
                if (!CheckIfPrime(result))
                {
                    result += 2;
                    continue;
                }
                yield return result;
                result += 2;
            }
        }

        private bool CheckIfPrime(long p)
        {
            for (var t = 3; t <= Math.Sqrt(p); t += 2)
            {
                if (p % t == 0)
                    return false;
            }

            return true;
        }
        #endregion
    }
}
