﻿using System;
using System.Timers;
using NumberUtilities;

namespace IntervalOutput
{
    public static class Program
    {
        private static PrimeGenerator _nu;
        public static void Main()
        {
            Start();

            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("Press the Enter key to exit the program at any time... ");
            Console.ReadLine();        
        }

        private static void Start()
        {
            _nu = new PrimeGenerator
            {
                LimitTimer = new Timer {Interval = 600000},
                IntervalTimer = new Timer {Interval = 1000}
            };

            //Wire up events to use results of prime calculations
            _nu.IntervalTimer.Elapsed += OnIntervalEvent;
            _nu.LimitTimer.Elapsed += OnEndEvent;

            //Enable the timers before starting the loop to gather primes
            _nu.IntervalTimer.Enabled = true;
            _nu.LimitTimer.Enabled = true;

            _nu.StartCalculatingPrimes();
        }

        private static void OnIntervalEvent(Object source, ElapsedEventArgs e)
        {
            Console.WriteLine($"{e.SignalTime} - Calculated primes up to: {_nu.LastPrime}");
        }

        private static void OnEndEvent(object sender, ElapsedEventArgs e)
        {
            _nu.IntervalTimer.Enabled = false;
            Console.WriteLine($"And the final prime number calculated was: {_nu.LastPrime}.");
            _nu.LimitTimer.Enabled = false;
        }
    }
}
